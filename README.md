Wavelet is a graph-based discrete constraint solver written in F#.

What does that mean?
It uses graphs to enforce arbitrary (user-defined) constraints on a set of elements that can be in distinct, discrete "states".