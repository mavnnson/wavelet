open WaveletOld
open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Running

printfn "Old graph tests \nFour Colour problem test"

type OldColour = Red | Green | Blue | Yellow

let OldCountry1 = { States = [|Red;Green;Blue;Yellow|] }
let OldCountry2 = { States = [|Red;Green;Yellow|] }
let OldCountry3 = { States = [|Red;Green|] }
let OldCountry4 = { States = [|Red|] }

let OldMapBorders = [|
                  { A = OldCountry1; B = OldCountry2 };
                  { A = OldCountry1; B = OldCountry3 };
                  { A = OldCountry1; B = OldCountry4 };
                  { A = OldCountry2; B = OldCountry3 };
                  { A = OldCountry2; B = OldCountry4 };
                  { A = OldCountry3; B = OldCountry4 };
             |]

let OldMapGraph = {
    Edges = OldMapBorders;
    Constraints = [| (fun a b -> not (a.Equals b)) |];
}

OldMapGraph.Solve ()

printfn "%A" OldCountry1.States
printfn "%A" OldCountry2.States
printfn "%A" OldCountry3.States
printfn "%A" OldCountry4.States

printfn "New graph tests \nFour Colour problem test"

open Wavelet

type Colour = Red | Green | Blue | Yellow

let Country1 = Node.fromStateArray [|Red;Green;Blue;Yellow|]
let Country2 = Node.fromStateArray [|Red;Green;Yellow|]
let Country3 = Node.fromStateArray [|Red;Green|]
let Country4 = Node.fromStateArray [|Red|]

Country1.Impacts <- [| Country2; Country3; Country4 |]
Country2.Impacts <- [| Country1; Country3; Country4 |]
Country3.Impacts <- [| Country2; Country1; Country4 |]
Country4.Impacts <- [| Country2; Country3; Country1 |]

let MapGraph = {
    Nodes = [| Country2; Country3; Country1; Country4 |];
    Constraints = [| (fun a b -> not (a.Equals b)) |];
}

MapGraph.Solve (Array.singleton Country4)

printfn "%A" Country1.tryGetState
printfn "%A" Country2.tryGetState
printfn "%A" Country3.tryGetState
printfn "%A" Country4.tryGetState

printfn "Chequerboard benchmark"

type Square = Black | White

type Benchmarks () =
    static GetOldChequerboard =
        let Squares = Array2D.init 1000 1000 (fun _ _ -> { States = [|Black;White|] })

        Squares[0,0] <- { States = [|Black|] }

        let GetBorders x y =
            if x>0 && y>0 then [|Squares[x-1,y];Squares[x,y-1]|]
            elif x>0 then [|Squares[x-1,y]|]
            elif y>0 then [|Squares[x,y-1]|]
            else [||]

        let SquareBorders = [|
            for x in 0 .. 999 do
                for y in 0 .. 999 do
                    let neighbors = GetBorders x y
                    yield! Array.map (fun n -> { A = n; B = Squares[x,y]} ) neighbors
            |]

        { Edges = SquareBorders; Constraints = [| (fun a b -> not (a.Equals b)) |] }

    static BuildChequerboard () =
        let Squares = Array2D.init 1000 1000 (fun _ _ -> Node.fromStateArray [| Black; White |])

        Squares[999, 999] <- Node.fromStateArray [| Black |]

        let GetBorders x y =
            if x > 0 && y > 0 then
                [| Squares[x - 1, y]; Squares[x, y - 1] |]
            elif x > 0 then
                [| Squares[x - 1, y] |]
            elif y > 0 then
                [| Squares[x, y - 1] |]
            else
                [||]

        let Nodes =
            [|  for x in 0..999 do
                    for y in 0..999 do
                        let neighbors = GetBorders x y
                        Squares[x,y].Impacts <- neighbors
                        yield Squares[x,y] |]
        { Nodes = Nodes
          Constraints = [| (fun a b -> not (a.Equals b)) |] }

    static BuildMultiColouredChequerboard () =
        let Squares = Array2D.init 1000 1000 (fun _ _ -> Node.fromStateArray [| Red; Green; Blue; Yellow |])

        Squares[999, 999] <- Node.fromStateArray [| Red |]

        let GetBorders x y =
            if x > 0 && y > 0 then
                [| Squares[x - 1, y]; Squares[x, y - 1] |]
            elif x > 0 then
                [| Squares[x - 1, y] |]
            elif y > 0 then
                [| Squares[x, y - 1] |]
            else
                [||]

        let Nodes =
            [| for x in 0..999 do
                   for y in 0..999 do
                       let neighbors = GetBorders x y
                       Squares[x, y].Impacts <- neighbors
                       yield Squares[x, y] |]

        { Nodes = Nodes
          Constraints = [| (fun a b -> a <> Red || b = Green)
                           (fun a b -> a <> Green || b = Blue)
                           (fun a b -> a <> Blue || b = Yellow)
                           (fun a b -> a <> Yellow || b = Red)
                         |] }

    [<Benchmark>]
    member this.OldChequerboard() =
        let myChequerboard = Benchmarks.GetOldChequerboard
        myChequerboard.Solve ()
    [<Benchmark>]
    member this.BuildOldChequerboard() =
        Benchmarks.GetOldChequerboard |> ignore
    [<Benchmark>]
    member this.Chequerboard() =
        let myChequerboard = Benchmarks.BuildChequerboard ()
        myChequerboard.Solve (Array.singleton myChequerboard.Nodes[myChequerboard.Nodes.Length-1])
    [<Benchmark>]
    member this.ParallelChequerboard() =
        let myChequerboard = Benchmarks.BuildChequerboard ()
        myChequerboard.ParallelSolve(Array.singleton myChequerboard.Nodes[myChequerboard.Nodes.Length - 1])
    [<Benchmark>]
    member this.ChequerboardBuild() =
        Benchmarks.BuildChequerboard () |> ignore
    [<Benchmark>]
    member this.MulticolouredChequerboard() =
        let myChequerboard = Benchmarks.BuildMultiColouredChequerboard ()
        myChequerboard.Solve (Array.singleton myChequerboard.Nodes[myChequerboard.Nodes.Length-1])
    [<Benchmark>]
    member this.MulticolouredParallelChequerboard() =
        let myChequerboard = Benchmarks.BuildMultiColouredChequerboard ()
        myChequerboard.ParallelSolve(Array.singleton myChequerboard.Nodes[myChequerboard.Nodes.Length - 1])
    [<Benchmark>]
    member this.MulticolouredChequerboardBuild() =
        Benchmarks.BuildMultiColouredChequerboard () |> ignore

BenchmarkRunner.Run<Benchmarks>() |> ignore
