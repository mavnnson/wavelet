﻿module Wavelet
open System

type Node<'State when 'State : equality> =
    { States : Option<'State>[]
      mutable Impacts : Node<'State>[] }

    static member fromStateArray stateArray =
        { States = [| for s in stateArray do yield Some s |]
          Impacts = [||] }

    member this.getPossible =
       [| for s in this.States do match s with Some s -> yield s | None -> () |]

    member this.tryGetState =
        let states = this.getPossible
        if states.Length = 1 then Some states[0]
        else None

type Graph<'State when 'State : equality and 'State : comparison> =
    { Nodes : Node<'State>[]
      Constraints : ('State -> 'State -> bool)[] }

    member this.Propagate startNode endNode =
            let mutable changed = false
            for i in 0 .. endNode.States.Length-1 do
                match endNode.States[i] with
                | Some endState ->
                    let mutable compatible = false
                    for startState in startNode.States do
                        match startState with
                        | Some startState ->
                            let incompatible =
                                this.Constraints
                                |> Array.map (fun c -> c startState endState)
                                |> Array.contains false
                            if not incompatible then compatible <- true
                        | None -> ()
                    if not compatible then
                        endNode.States[i] <- None
                        changed <- true
                | None -> ()
            changed

    member this.Solve nodesToUpdate =

        let nextRound =
            [|
             for node in nodesToUpdate do
                for impactedNode in node.Impacts do
                    if this.Propagate node impactedNode then
                        yield impactedNode
             |]

        if nextRound.Length <> 0 then this.Solve nextRound

    member this.ParallelSolve nodesToUpdate =

        let updateNode node =
            [| for impactedNode in node.Impacts do
                   if this.Propagate node impactedNode then
                       yield impactedNode |]

        let nextRound =
            nodesToUpdate
            |> Array.Parallel.map updateNode
            |> Array.concat

        if nextRound.Length <> 0 then
            this.ParallelSolve nextRound

