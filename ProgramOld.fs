﻿module WaveletOld

type Node<'State> =
    { mutable States : 'State[] }

type UnorientedEdge<'State> =
    { A : Node<'State>
      B : Node<'State> }

type Edge<'State> =
    | Unoriented of UnorientedEdge<'State>

type Graph<'State when 'State : equality and 'State : comparison> =
    { Edges : UnorientedEdge<'State>[]
      Constraints : ('State -> 'State -> bool)[] }

    member this.Solve () =
        let mutable changed = false
        let updateNode e =
            let mutable changed = false
            let compatibilityTable = Array2D.init e.A.States.Length e.B.States.Length (fun a b -> (Array.reduce (fun a b -> a && b) (Array.map (fun c -> c e.A.States[a] e.B.States[b]) this.Constraints ) ) )

            e.A.States <- [|
                 for i in 0 .. e.A.States.Length - 1 do
                    let compatibility =
                        compatibilityTable[i, *]
                        |> Array.contains true
                    if compatibility then
                        yield e.A.States[i]
                    else changed <- true
                |]

            e.B.States <- [|
                 for i in 0 .. e.B.States.Length - 1 do
                    let compatibility =
                        compatibilityTable[*, i]
                        |> Array.contains true
                    if compatibility then
                        yield e.B.States[i]
                    else changed <- true
                |]
            changed

        changed <- this.Edges
        |> Array.map updateNode
        |> Array.contains true

        if changed then this.Solve ()
